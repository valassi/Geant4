/** \mainpage notitle
 *  \anchor geant4doxygenmain
 *
 * This is the code reference manual for the Geant4 classes.
 
 * These pages have been generated directly from the code and reflect the 
 * state of the software for this LHCb build of the Geant4 packages. More
 * information is available from the 
 * <a href="http://cern.ch/LHCb-release-area/DOC/geant4/">web pages</a>
 * of the LHCb build of the Geant4 project
 *
 */
