gaudi_subdir(LHCbG4PhysLists v1r0)

gaudi_depends_on_subdirs(Geant4/G4config)

find_package(CLHEP REQUIRED COMPONENTS Vector)

include_directories(SYSTEM ${CMAKE_INSTALL_PREFIX}/include ${CLHEP_INCLUDE_DIRS})
link_directories(${CMAKE_INSTALL_PREFIX}/lib)

set(Geant4_LIBRARIES
  -lG4global
  -lG4physicslists
  -lG4processes
  -lG4particles
  -lG4persistency
  -lG4run)

gaudi_add_library(G4LHCblists
                  src/*.cc
                  PUBLIC_HEADERS LHCbG4PhysLists
                  INCLUDE_DIRS CLHEP
                  LINK_LIBRARIES ${Geant4_LIBRARIES} CLHEP)

add_dependencies(G4LHCblists Geant4)
