2018-04-25 Geant4 v104r1
===
This version uses LCG 91 (Gaudi v29r0) and Geant4Files v104r0.

This version is released on `Sim10` branch.

## Latest Changes

#### GEANT4

Switched to Geant4 release `10.4.1`.

#### LHCb G4 Physics Lists

**[MR !31] Updated method name in PhysLists for 10.4.1**

#### LHCb G4 Tests

**[MR !31] Added new test G4GammaToDiLeptonConversionTest based on TestEm6**
