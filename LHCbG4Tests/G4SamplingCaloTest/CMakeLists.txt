gaudi_subdir(G4SamplingCaloTest v1r0)

gaudi_depends_on_subdirs(Geant4/G4config)
gaudi_depends_on_subdirs(LHCbG4PhysLists)

find_package(CLHEP REQUIRED)

include_directories(SYSTEM ${CMAKE_INSTALL_PREFIX}/include ${CLHEP_INCLUDE_DIRS})
link_directories(${CMAKE_INSTALL_PREFIX}/lib)

set(Geant4_LIBRARIES
    -lG4analysis
    -lG4physicslists
    -lG4intercoms
    -lG4event
    -lG4tracking
    -lG4track
    -lG4geometry
    -lG4materials)

gaudi_add_executable(G4SamplingCaloTest
                     G4SamplingCaloTest.cc src/*.cc
                     INCLUDE_DIRS include CLHEP
                     LINK_LIBRARIES ${Geant4_LIBRARIES} CLHEP G4LHCblists)

add_dependencies(G4SamplingCaloTest Geant4)

gaudi_install_scripts()
