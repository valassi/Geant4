#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import os
import subprocess

"""
	Stirring application for the Sampling Calorimeter test (aka TestEm3).
"""


def create_cfg_file(n_evts=10000, name='G4SamplingCaloTest.mac'):
	"""
		Create the stirring file with Geant4 directives
	"""
	with open('G4SamplingCaloTest.mac', 'w') as outfile:
		macro_payload = ('# Macro file for Calo test (Em3)\n' +
										 '#\n' +
										 '# LHCB ECAL model\n' +
										 '#\n' +
										 '/control/verbose 2\n' +
										 '/run/verbose 1\n' +
										 '#\n' +
										 '/testem/det/setNbOfLayers 66\n' +
										 '/testem/det/setNbOfAbsor  2\n' +
										 '/testem/det/setAbsor 1 Lead         2 mm\n' +
										 '/testem/det/setAbsor 2 Scintillator 4 mm\n' +
										 '/testem/det/setSizeYZ 20 cm\n' +
										 '#\n' +
										 '/testem/phys/addPhysics emstandard_opt1nocuts\n' +
										 '#\n' +
										 '#/gun/particle e-\n' +
										 '#/gun/energy 100 GeV\n' +
										 '#\n' +
										 '#/testem/event/printModulo 100\n' +
										 '#\n' +
										 '#/run/setCut 5.0 mm\n' +
										 '#/run/initialize\n' +
										 '#/analysis/setFileName NOUTPUT_1\n' +
										 '#/analysis/h1/set 1 500	  60. 100. GeV\n' +
										 '#/analysis/h1/set 2 500   5.  35. GeV\n' +
										 '#/analysis/h1/set 11 66   0.   66.  none #long. profile in absor1\n' +
										 '#/analysis/h1/set 12 66   0.   66.  none #long. profile in absor2\n' +
										 '#/analysis/h1/set 21 102  0.   102. none #energy flow\n' +
										 '#/analysis/h1/set 22 102  0.   102. none #lateral energy leakage\n' +
										 '#/run/beamOn 10000\n' +
										 '#\n' +
										 '/gun/particle e-\n' +
										 '/gun/energy 44.44 GeV\n' +
										 '#\n' +
										 '/run/setCut 5.0 mm\n' +
										 '/run/initialize\n' +
										 '/analysis/setFileName NOUTPUT_2\n' +
										 '/analysis/h1/set 1 500	  26. 45. GeV\n' +
										 '/analysis/h1/set 2 500   2.22  16. GeV\n' +
										 '/analysis/h1/set 11 66   0.   66.  none #long. profile in absor1\n' +
										 '/analysis/h1/set 12 66   0.   66.  none #long. profile in absor2\n' +
										 '/analysis/h1/set 21 102  0.   102. none #energy flow\n' +
										 '/analysis/h1/set 22 102  0.   102. none #lateral energy leakage\n' +
										 '/analysis/h1/setActivation 23 true\n' +
										 '/analysis/h1/setActivation 24 true\n' +
										 '/analysis/h1/setActivation 25 true\n' +
										 '/analysis/h1/setActivation 26 true\n' +
										 '/analysis/h1/setActivation 27 true\n' +
										 '/analysis/h1/setActivation 28 true\n' +
										 '/run/beamOn 10000\n' +
										 '#\n' +
										 '/gun/particle e-\n' +
										 '/gun/energy 25.0 GeV\n' +
										 '#\n' +
										 '/run/setCut 5.0 mm\n' +
										 '/run/initialize\n' +
										 '/analysis/setFileName NOUTPUT_3\n' +
										 '/analysis/h1/set 1 500	  15.00 25.00 GeV\n' +
										 '/analysis/h1/set 2 500   1.25  8.75 GeV\n' +
										 '/analysis/h1/set 11 66   0.   66.  none #long. profile in absor1\n' +
										 '/analysis/h1/set 12 66   0.   66.  none #long. profile in absor2\n' +
										 '/analysis/h1/set 21 102  0.   102. none #energy flow\n' +
										 '/analysis/h1/set 22 102  0.   102. none #lateral energy leakage\n' +
										 '/analysis/h1/setActivation 23 true\n' +
										 '/analysis/h1/setActivation 24 true\n' +
										 '/analysis/h1/setActivation 25 true\n' +
										 '/analysis/h1/setActivation 26 true\n' +
										 '/analysis/h1/setActivation 27 true\n' +
										 '/analysis/h1/setActivation 28 true\n' +
										 '/run/beamOn 10000\n' +
										 '#\n' +
										 '/gun/particle e-\n' +
										 '/gun/energy 16.00 GeV\n' +
										 '#\n' +
										 '/run/setCut 5.0 mm\n' +
										 '/run/initialize\n' +
										 '/analysis/setFileName NOUTPUT_4\n' +
										 '/analysis/h1/set 1 500	  9.60 16.00 GeV\n' +
										 '/analysis/h1/set 2 500   0.80  5.60 GeV\n' +
										 '/analysis/h1/set 11 66   0.   66.  none #long. profile in absor1\n' +
										 '/analysis/h1/set 12 66   0.   66.  none #long. profile in absor2\n' +
										 '/analysis/h1/set 21 102  0.   102. none #energy flow\n' +
										 '/analysis/h1/set 22 102  0.   102. none #lateral energy leakage\n' +
										 '/analysis/h1/setActivation 23 true\n' +
										 '/analysis/h1/setActivation 24 true\n' +
										 '/analysis/h1/setActivation 25 true\n' +
										 '/analysis/h1/setActivation 26 true\n' +
										 '/analysis/h1/setActivation 27 true\n' +
										 '/analysis/h1/setActivation 28 true\n' +
										 '/run/beamOn 10000\n' +
										 '#\n' +
										 '/gun/particle e-\n' +
										 '/gun/energy 11.11 GeV\n' +
										 '#\n' +
										 '/run/setCut 5.0 mm\n' +
										 '/run/initialize\n' +
										 '/analysis/setFileName NOUTPUT_5\n' +
										 '/analysis/h1/set 1 500	  6.67 11.11 GeV\n' +
										 '/analysis/h1/set 2 500   0.56  3.89 GeV\n' +
										 '/analysis/h1/set 11 66   0.   66.  none #long. profile in absor1\n' +
										 '/analysis/h1/set 12 66   0.   66.  none #long. profile in absor2\n' +
										 '/analysis/h1/set 21 102  0.   102. none #energy flow\n' +
										 '/analysis/h1/set 22 102  0.   102. none #lateral energy leakage\n' +
										 '/analysis/h1/setActivation 23 true\n' +
										 '/analysis/h1/setActivation 24 true\n' +
										 '/analysis/h1/setActivation 25 true\n' +
										 '/analysis/h1/setActivation 26 true\n' +
										 '/analysis/h1/setActivation 27 true\n' +
										 '/analysis/h1/setActivation 28 true\n' +
										 '/run/beamOn 10000\n' +
										 '#\n' +
										 '/gun/particle e-\n' +
										 '/gun/energy 8.16 GeV\n' +
										 '#\n' +
										 '/run/setCut 5.0 mm\n' +
										 '/run/initialize\n' +
										 '/analysis/setFileName NOUTPUT_6\n' +
										 '/analysis/h1/set 1 500	  4.90 8.16 GeV\n' +
										 '/analysis/h1/set 2 500   0.41  2.86 GeV\n' +
										 '/analysis/h1/set 11 66   0.   66.  none #long. profile in absor1\n' +
										 '/analysis/h1/set 12 66   0.   66.  none #long. profile in absor2\n' +
										 '/analysis/h1/set 21 102  0.   102. none #energy flow\n' +
										 '/analysis/h1/set 22 102  0.   102. none #lateral energy leakage\n' +
										 '/analysis/h1/setActivation 23 true\n' +
										 '/analysis/h1/setActivation 24 true\n' +
										 '/analysis/h1/setActivation 25 true\n' +
										 '/analysis/h1/setActivation 26 true\n' +
										 '/analysis/h1/setActivation 27 true\n' +
										 '/analysis/h1/setActivation 28 true\n' +
										 '/run/beamOn 10000\n' +
										 '#\n' +
										 '/gun/particle e-\n' +
										 '/gun/energy 6.25 GeV\n' +
										 '#\n' +
										 '/run/setCut 5.0 mm\n' +
										 '/run/initialize\n' +
										 '/analysis/setFileName NOUTPUT_7\n' +
										 '/analysis/h1/set 1 500	  3.75 6.25 GeV\n' +
										 '/analysis/h1/set 2 500   0.31  2.19 GeV\n' +
										 '/analysis/h1/set 11 66   0.   66.  none #long. profile in absor1\n' +
										 '/analysis/h1/set 12 66   0.   66.  none #long. profile in absor2\n' +
										 '/analysis/h1/set 21 102  0.   102. none #energy flow\n' +
										 '/analysis/h1/set 22 102  0.   102. none #lateral energy leakage\n' +
										 '/analysis/h1/setActivation 23 true\n' +
										 '/analysis/h1/setActivation 24 true\n' +
										 '/analysis/h1/setActivation 25 true\n' +
										 '/analysis/h1/setActivation 26 true\n' +
										 '/analysis/h1/setActivation 27 true\n' +
										 '/analysis/h1/setActivation 28 true\n' +
										 '/run/beamOn 10000\n' +
										 '#\n' +
										 '/gun/particle e-\n' +
										 '/gun/energy 4.94 GeV\n' +
										 '#\n' +
										 '/run/setCut 5.0 mm\n' +
										 '/run/initialize\n' +
										 '/analysis/setFileName NOUTPUT_8\n' +
										 '/analysis/h1/set 1 500	  2.96 4.94 GeV\n' +
										 '/analysis/h1/set 2 500   0.25  1.73 GeV\n' +
										 '/analysis/h1/set 11 66   0.   66.  none #long. profile in absor1\n' +
										 '/analysis/h1/set 12 66   0.   66.  none #long. profile in absor2\n' +
										 '/analysis/h1/set 21 102  0.   102. none #energy flow\n' +
										 '/analysis/h1/set 22 102  0.   102. none #lateral energy leakage\n' +
										 '/analysis/h1/setActivation 23 true\n' +
										 '/analysis/h1/setActivation 24 true\n' +
										 '/analysis/h1/setActivation 25 true\n' +
										 '/analysis/h1/setActivation 26 true\n' +
										 '/analysis/h1/setActivation 27 true\n' +
										 '/analysis/h1/setActivation 28 true\n' +
										 '/run/beamOn 10000\n' +
										 '#\n' +
										 '/gun/particle e-\n' +
										 '/gun/energy 4.00 GeV\n' +
										 '#\n' +
										 '/run/setCut 5.0 mm\n' +
										 '/run/initialize\n' +
										 '/analysis/setFileName NOUTPUT_9\n' +
										 '/analysis/h1/set 1 500	  2.4 4.0 GeV\n' +
										 '/analysis/h1/set 2 500   0.2  1.4 GeV\n' +
										 '/analysis/h1/set 11 66   0.   66.  none #long. profile in absor1\n' +
										 '/analysis/h1/set 12 66   0.   66.  none #long. profile in absor2\n' +
										 '/analysis/h1/set 21 102  0.   102. none #energy flow\n' +
										 '/analysis/h1/set 22 102  0.   102. none #lateral energy leakage\n' +
										 '/analysis/h1/setActivation 23 true\n' +
										 '/analysis/h1/setActivation 24 true\n' +
										 '/analysis/h1/setActivation 25 true\n' +
										 '/analysis/h1/setActivation 26 true\n' +
										 '/analysis/h1/setActivation 27 true\n' +
										 '/analysis/h1/setActivation 28 true\n' +
										 '/run/beamOn 10000\n' +
										 '#\n' +
										 '/gun/particle e-\n' +
										 '/gun/energy 3.31 GeV\n' +
										 '#\n' +
										 '/run/setCut 5.0 mm\n' +
										 '/run/initialize\n' +
										 '/analysis/setFileName NOUTPUT_10\n' +
										 '/analysis/h1/set 1 500	  1.98 3.31 GeV\n' +
										 '/analysis/h1/set 2 500   0.17  1.16 GeV\n' +
										 '/analysis/h1/set 11 66   0.   66.  none #long. profile in absor1\n' +
										 '/analysis/h1/set 12 66   0.   66.  none #long. profile in absor2\n' +
										 '/analysis/h1/set 21 102  0.   102. none #energy flow\n' +
										 '/analysis/h1/set 22 102  0.   102. none #lateral energy leakage\n' +
										 '/analysis/h1/setActivation 23 true\n' +
										 '/analysis/h1/setActivation 24 true\n' +
										 '/analysis/h1/setActivation 25 true\n' +
										 '/analysis/h1/setActivation 26 true\n' +
										 '/analysis/h1/setActivation 27 true\n' +
										 '/analysis/h1/setActivation 28 true\n' +
										 '/run/beamOn 10000\n' +
										 '#\n' +
										 '/gun/particle e-\n' +
										 '/gun/energy 2.78 GeV\n' +
										 '#\n' +
										 '/run/setCut 5.0 mm\n' +
										 '/run/initialize\n' +
										 '/analysis/setFileName NOUTPUT_11\n' +
										 '/analysis/h1/set 1 500	  1.67 2.78 GeV\n' +
										 '/analysis/h1/set 2 500   0.14  0.97 GeV\n' +
										 '/analysis/h1/set 11 66   0.   66.  none #long. profile in absor1\n' +
										 '/analysis/h1/set 12 66   0.   66.  none #long. profile in absor2\n' +
										 '/analysis/h1/set 21 102  0.   102. none #energy flow\n' +
										 '/analysis/h1/set 22 102  0.   102. none #lateral energy leakage\n' +
										 '/analysis/h1/setActivation 23 true\n' +
										 '/analysis/h1/setActivation  24 true\n' +
										 '/analysis/h1/setActivation  25 true\n' +
										 '/analysis/h1/setActivation  26 true\n' +
										 '/analysis/h1/setActivation  27 true\n' +
										 '/analysis/h1/setActivation  28 true\n' +
										 '/run/beamOn 10000\n' +
										 '#\n' +
										 '/gun/particle e-\n' +
										 '/gun/energy 2.37 GeV\n' +
										 '#\n' +
										 '/run/setCut 5.0 mm\n' +
										 '/run/initialize\n' +
										 '/analysis/setFileName NOUTPUT_12\n' +
										 '/analysis/h1/set 1 500	  1.42 2.37 GeV\n' +
										 '/analysis/h1/set 2 500   0.12  0.83 GeV\n' +
										 '/analysis/h1/set 11 66   0.   66.  none #long. profile in absor1\n' +
										 '/analysis/h1/set 12 66   0.   66.  none #long. profile in absor2\n' +
										 '/analysis/h1/set 21 102  0.   102. none #energy flow\n' +
										 '/analysis/h1/set 22 102  0.   102. none #lateral energy leakage\n' +
										 '/analysis/h1/setActivation  23 true\n' +
										 '/analysis/h1/setActivation  24 true\n' +
										 '/analysis/h1/setActivation  25 true\n' +
										 '/analysis/h1/setActivation  26 true\n' +
										 '/analysis/h1/setActivation  27 true\n' +
										 '/analysis/h1/setActivation  28 true\n' +
										 '/run/beamOn 10000\n' +
										 '#\n' +
										 '/gun/particle e-\n' +
										 '/gun/energy 2.04 GeV\n' +
										 '#\n' +
										 '/run/setCut 5.0 mm\n' +
										 '/run/initialize\n' +
										 '/analysis/setFileName NOUTPUT_13\n' +
										 '/analysis/h1/set 1 500	  1.22 2.04 GeV\n' +
										 '/analysis/h1/set 2 500   0.1  0.71 GeV\n' +
										 '/analysis/h1/set 11 66   0.   66.  none #long. profile in absor1\n' +
										 '/analysis/h1/set 12 66   0.   66.  none #long. profile in absor2\n' +
										 '/analysis/h1/set 21 102  0.   102. none #energy flow\n' +
										 '/analysis/h1/set 22 102  0.   102. none #lateral energy leakage\n' +
										 '\n' +
										 '/run/beamOn 10000\n' +
										 '#\n' +
										 '/gun/particle e-\n' +
										 '/gun/energy 1.78 GeV\n' +
										 '#\n' +
										 '/run/setCut 5.0 mm\n' +
										 '/run/initialize\n' +
										 '/analysis/setFileName NOUTPUT_14\n' +
										 '/analysis/h1/set 1 500	  1.07 1.78 GeV\n' +
										 '/analysis/h1/set 2 500   0.09  0.62 GeV\n' +
										 '/analysis/h1/set 11 66   0.   66.  none #long. profile in absor1\n' +
										 '/analysis/h1/set 12 66   0.   66.  none #long. profile in absor2\n' +
										 '/analysis/h1/set 21 102  0.   102. none #energy flow\n' +
										 '/analysis/h1/set 22 102  0.   102. none #lateral energy leakage\n' +
										 '/analysis/h1/setActivation  23 true\n' +
										 '/analysis/h1/setActivation  24 true\n' +
										 '/analysis/h1/setActivation  25 true\n' +
										 '/analysis/h1/setActivation  26 true\n' +
										 '/analysis/h1/setActivation  27 true\n' +
										 '/analysis/h1/setActivation  28 true\n' +
										 '/run/beamOn 10000\n' +
										 '#\n' +
										 '/gun/particle e-\n' +
										 '/gun/energy 1.56 GeV\n' +
										 '#\n' +
										 '/run/setCut 5.0 mm\n' +
										 '/run/initialize\n' +
										 '/analysis/setFileName NOUTPUT_15\n' +
										 '/analysis/h1/set 1 500	  0.94 1.56 GeV\n' +
										 '/analysis/h1/set 2 500   0.08  0.55 GeV\n' +
										 '/analysis/h1/set 11 66   0.   66.  none #long. profile in absor1\n' +
										 '/analysis/h1/set 12 66   0.   66.  none #long. profile in absor2\n' +
										 '/analysis/h1/set 21 102  0.   102. none #energy flow\n' +
										 '/analysis/h1/set 22 102  0.   102. none #lateral energy leakage\n' +
										 '/analysis/h1/setActivation  23 true\n' +
										 '/analysis/h1/setActivation  24 true\n' +
										 '/analysis/h1/setActivation  25 true\n' +
										 '/analysis/h1/setActivation  26 true\n' +
										 '/analysis/h1/setActivation  27 true\n' +
										 '/analysis/h1/setActivation  28 true\n' +
										 '/run/beamOn 10000\n')

		# This could be activated to change the number of events,
		# but then also requires changing the number in the post processing
		# app - G4SamplingCaloTest_postprocess.C
		#if not n_evts == 10000:
		#	macro_payload = macro_payload.replace('/run/beamOn 10000', ('/run/beamOn ' + str(n_evts)))

		outfile.write(macro_payload)


def main():
	args_parser = argparse.ArgumentParser(
		description='Stirring application for the Sampling Calorimeter test (aka TestEm3).')

	args_parser.add_argument('n_evts',
													 help='Number of events (int)',
													 nargs='?',
													 action='store',
													 type=int,
													 default=10000)

	parsed_args, leftovers = args_parser.parse_known_args()

	print 'Starting Sampling Calorimeter test with ' + str(parsed_args.n_evts) + ' event(s)...'

	workspace = os.path.join(os.sep, os.getcwd(), 'G4SamplingCaloTestOutput')
	if not os.path.exists(workspace):
		os.makedirs(workspace)

	os.chdir(workspace)
	print 'Working directory:', os.getcwd()

	cfg_file_name = 'G4SamplingCaloTest.mac'

	print 'Creating the Geant4 directives file...'
	create_cfg_file(n_evts=parsed_args.n_evts, name=cfg_file_name)

	print 'Executing the test...'
	cmd = ' '.join(['G4SamplingCaloTest.exe', cfg_file_name])
	subprocess.call(cmd, shell=True)

	# Do not compile, run via ROOT directly
	#print 'Compiling the post-processing application...'
	#cmd = ' '.join(['g++', '-c', '`root-config --cflags`',
	#								'$G4SAMPLINGCALOTESTROOT/G4SamplingCaloTest_postprocess.C'])
	#subprocess.call(cmd, shell=True)
	#cmd = ' '.join(['g++', '-o', 'G4SamplingCaloTest_postprocess', '`root-config --glibs`',
	#								'G4SamplingCaloTest_postprocess.o'])
	#subprocess.call(cmd, shell=True)
	#subprocess.call('./G4SamplingCaloTest_postprocess', shell=True)

	print 'Running the post-processing application...'
	subprocess.call('root -l -b -q $G4SAMPLINGCALOTESTROOT/G4SamplingCaloTest_postprocess.C', shell=True)

	print "Results produced in Save.root, Selectedresults.root and selectedresults.txt"


if __name__ == '__main__':
	main()

#EOF
