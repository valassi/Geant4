// BASED ON TESTEM6 BY THE GEANT4 COLLABORATION
//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
/// \file electromagnetic/TestEm6/TestEm6.cc
/// \brief Main program of the electromagnetic/TestEm6 example
//
// $Id: TestEm6.cc 83428 2014-08-21 15:46:01Z gcosmo $
// 
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#include "Geant4/G4RunManager.hh"
#include "Geant4/G4UImanager.hh"
#include "Geant4/Randomize.hh"
#include "Geant4/G4GammaConversionToMuons.hh"
#include "Geant4/G4ProcessTable.hh"

#include "DetectorConstruction.hh"
#include "PrimaryGeneratorAction.hh"
#include "SteppingVerbose.hh"

#include "PhysicsList.hh"

#include "RunAction.hh"
#include "SteppingAction.hh"
#include "StackingAction.hh"

#ifdef G4VIS_USE
 #include "G4VisExecutive.hh"
#endif

#ifdef G4UI_USE
#include "G4UIExecutive.hh"
#endif

G4double energy = 1000; //in GeV

G4int nEvts = 100;

G4double thickness = 0.3; //in mm

G4String dimu_xsec_fac = "1000";

G4String output_dir = ".";

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

 
int main(int argc,char** argv) {

  for(int i{0}; i<argc; ++i)
  {
       if(G4String(argv[i]) == "--nevts")
       {
          ++i;
          nEvts = G4int(atoi(argv[i]));
          ++i;
       }
       if(G4String(argv[i]) == "--energy")
       {
          ++i;
          energy = G4double(atof(argv[i]));
          ++i;
       }

       if(G4String(argv[i]) == "--thickness")
       {
          ++i;
          thickness = G4double(atof(argv[i]));
          ++i;
       }
       if(G4String(argv[i]) == "--scale")
       {
          ++i;
          dimu_xsec_fac = G4String(argv[i]);
          ++i;
       }

       if(G4String(argv[i]) == "--outputdir")
       {
          ++i;
          output_dir = G4String(argv[i]);
          ++i;
       }

  }

  //choose the Random engine
  CLHEP::HepRandom::setTheEngine(new CLHEP::RanecuEngine);
  
  //my Verbose output class
  G4VSteppingVerbose::SetInstance(new SteppingVerbose);
    
  //Construct the default run manager
  G4RunManager * runManager = new G4RunManager;

  //Set Physics List
  runManager->SetUserInitialization(new PhysicsList);     

  //set mandatory initialization classes
  DetectorConstruction* det;
  runManager->SetUserInitialization(det = new DetectorConstruction(thickness));
  runManager->SetUserAction(new PrimaryGeneratorAction(det, energy, nEvts));
    
  //set user action classes
  RunAction* RunAct;
  
  runManager->SetUserAction(RunAct = new RunAction(det, energy, thickness, nEvts, output_dir)); 
  runManager->SetUserAction(new SteppingAction(RunAct));
  runManager->SetUserAction(new StackingAction);

  //get the pointer to the User Interface manager 
  G4UImanager* UI = G4UImanager::GetUIpointer();  

  
  // Initialize Run Manager

  UI->ApplyCommand("/run/initialize");
 

  // Increase Gamma2MuMu XSec to 1000x
  
  UI->ApplyCommand("/testem/phys/SetGammaToMuPairFac "+dimu_xsec_fac);  

  //Start Simulation
  UI->ApplyCommand("/run/beamOn");
 
  //job termination
  delete runManager;

  return 0;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
