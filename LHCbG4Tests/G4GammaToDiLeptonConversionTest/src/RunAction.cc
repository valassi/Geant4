//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
/// \file electromagnetic/TestEm6/src/RunAction.cc
/// \brief Implementation of the RunAction class
//
// $Id: RunAction.cc 83428 2014-08-21 15:46:01Z gcosmo $
// 
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#include "Geant4/G4SystemOfUnits.hh"  
#include "Geant4/G4PhysicalConstants.hh"  
#include "Geant4/G4EmCalculator.hh" 
#include "Geant4/G4ParticleTable.hh" 
#include "Geant4/G4ParticleDefinition.hh" 
#include "Geant4/G4Positron.hh" 
#include "Geant4/G4AnnihiToMuPair.hh"  
#include "Geant4/G4eeToHadrons.hh"  
#include "Geant4/G4eeToHadronsModel.hh"  
#include "Geant4/G4eBremsstrahlung.hh"  

#include "RunAction.hh"
#include "Geant4/G4Run.hh"
#include "Geant4/G4RunManager.hh"
#include "Geant4/G4MuonMinus.hh"

#include <sstream>

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

RunAction::RunAction(DetectorConstruction* det, G4double e, G4double t, G4int n, G4String dir)
 : G4UserRunAction(),fDetector(det),fProcCounter(0),fAnalysis(0),fMat(0)
{
  std::stringstream file_name;
  file_name << dir;
  file_name << "/TestEM6_Al_";
  file_name << t;
  file_name << "mm_";
  file_name << e;
  file_name << "GeV_";
  file_name << n;
  file_name << ".root";

  fileName = file_name.str();

  fMinE = 40*GeV;
  fMaxE = 10000*GeV;
  fnBin = 10000;
  fNtColId[0] = fNtColId[1] = fNtColId[2] = 0;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

RunAction::~RunAction()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void RunAction::BeginOfRunAction(const G4Run* aRun)
{  
  G4cout << "### Run " << aRun->GetRunID() << " start." << G4endl;

  //get material
  //
  fMat = fDetector->GetMaterial();
  G4cout << "###RunAction::BeginOfRunAction  Material:" 
         << fMat->GetName() << G4endl;
 
  fProcCounter = new ProcessesCount;

  fAnalysis = G4AnalysisManager::Instance();
  
  // Open an output file
  //
  fAnalysis->OpenFile(fileName);    
  fAnalysis->SetVerboseLevel(2);

  //===========================================================
  
  fAnalysis->CreateNtuple("DiMuon", "DiMuon Production");
  fAnalysis->CreateNtupleDColumn("DiMuon_EnergyFracPlus");
  fAnalysis->CreateNtupleDColumn("DiMuon_EnergyFracMinus");
  fAnalysis->CreateNtupleDColumn("DiMuon_ThetaPlus");
  fAnalysis->CreateNtupleDColumn("DiMuon_ThetaMinus");
  fAnalysis->CreateNtupleDColumn("DiMuon_InvMass");
  fAnalysis->CreateNtupleDColumn("DiMuon_PX_Plus");
  fAnalysis->CreateNtupleDColumn("DiMuon_PY_Plus");
  fAnalysis->CreateNtupleDColumn("DiMuon_PZ_Plus");
  fAnalysis->CreateNtupleDColumn("DiMuon_PX_Minus");
  fAnalysis->CreateNtupleDColumn("DiMuon_PY_Minus");
  fAnalysis->CreateNtupleDColumn("DiMuon_PZ_Minus");
  fAnalysis->CreateNtupleDColumn("DiMuon_ThetaSep"); //Separation Angle Between DiLeptons
  fAnalysis->FinishNtuple();
 
  fAnalysis->CreateNtuple("DiElectron", "DiElectron Production");
  fAnalysis->CreateNtupleDColumn("DiElectron_EnergyFracPlus");
  fAnalysis->CreateNtupleDColumn("DiElectron_EnergyFracMinus");
  fAnalysis->CreateNtupleDColumn("DiElectron_ThetaPlus");
  fAnalysis->CreateNtupleDColumn("DiElectron_ThetaMinus");
  fAnalysis->CreateNtupleDColumn("DiElectron_InvMass");
  fAnalysis->CreateNtupleDColumn("DiElectron_PX_Plus");
  fAnalysis->CreateNtupleDColumn("DiElectron_PY_Plus");
  fAnalysis->CreateNtupleDColumn("DiElectron_PZ_Plus");
  fAnalysis->CreateNtupleDColumn("DiElectron_PX_Minus");
  fAnalysis->CreateNtupleDColumn("DiElectron_PY_Minus");
  fAnalysis->CreateNtupleDColumn("DiElectron_PZ_Minus");
  fAnalysis->CreateNtupleDColumn("DiElectron_ThetaSep"); //Separation Angle Between DiLeptons
  fAnalysis->FinishNtuple();

  G4cout << "\n----> NTuple file is opened in " << fileName << G4endl;   
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void RunAction::CountProcesses(G4String procName)
{
  //does the process  already encounted ?
  //
  size_t nbProc = fProcCounter->size();
  size_t i = 0;
  while ((i<nbProc)&&((*fProcCounter)[i]->GetName()!=procName)) i++;
  if (i == nbProc) fProcCounter->push_back( new OneProcessCount(procName));
  
  (*fProcCounter)[i]->Count();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void RunAction::EndOfRunAction(const G4Run*)
{
  G4double countTot = 0.;
  G4cout << "\n Number of process calls --->";
  for (size_t i=0; i< fProcCounter->size();i++) {
        G4String procName = (*fProcCounter)[i]->GetName();
        if (procName != "Transportation") {
          G4int count    = (*fProcCounter)[i]->GetCounter(); 
          G4cout << "\t" << procName << " : " << count;
          countTot += count;
        }
  }
 
  fAnalysis->Write();
  fAnalysis->CloseFile();

  delete fAnalysis;
  G4cout << G4endl;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
