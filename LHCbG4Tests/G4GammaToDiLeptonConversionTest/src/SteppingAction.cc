//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
/// \file electromagnetic/TestEm6/src/SteppingAction.cc
/// \brief Implementation of the SteppingAction class
//
// $Id: SteppingAction.cc 83428 2014-08-21 15:46:01Z gcosmo $
//
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#include "SteppingAction.hh"
#include "RunAction.hh"
#include "Geant4/G4SteppingManager.hh"
#include "Geant4/G4VProcess.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

SteppingAction::SteppingAction(RunAction* RuAct)
:G4UserSteppingAction(),fRunAction(RuAct)
{ 
 fMuonMass = G4MuonPlus::MuonPlus()->GetPDGMass();
 fEMass = G4Positron::Positron()->GetPDGMass();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

SteppingAction::~SteppingAction()
{ }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void SteppingAction::UserSteppingAction(const G4Step* aStep)
{
 const G4VProcess* process = aStep->GetPostStepPoint()->GetProcessDefinedStep();
 if (process == 0) return;  
 G4String processName = process->GetProcessName();
 fRunAction->CountProcesses(processName); //count processes
  
 if (processName != "GammaToMuPair" && processName != "conv") return;
 
 G4StepPoint* PrePoint = aStep->GetPreStepPoint();  
 G4double      EGamma  = PrePoint->GetTotalEnergy();
 G4ThreeVector PGamma  = PrePoint->GetMomentum();
    
 G4double      Eplus(0), Eminus(0), ETot(0);
 G4ThreeVector Pplus   , Pminus,    PTot; 
 const G4TrackVector* secondary = fpSteppingManager->GetSecondary();
 
 G4double particle_mass = -1;
 tuple_index = -1;

 if(processName == "conv")
 {  
     particle_mass = fEMass; 
     tuple_index = 1;
 }
 else
 {  
     particle_mass = fMuonMass; 
     tuple_index = 0;
 }

 for (size_t lp=0; lp<(*secondary).size(); lp++) {
   if ((*secondary)[lp]->GetDefinition()== muplus_def || (*secondary)[lp]->GetDefinition()== eplus_def) {
     Eplus  = (*secondary)[lp]->GetTotalEnergy();
     Pplus  = (*secondary)[lp]->GetMomentum();
   } else {
     Eminus = (*secondary)[lp]->GetTotalEnergy();
     Pminus = (*secondary)[lp]->GetMomentum();
   }
 
   ETot += (*secondary)[lp]->GetTotalEnergy();
   PTot += (*secondary)[lp]->GetMomentum();
 }
 
 G4double thetaSep = Pplus.angle(Pminus);              
 G4double xPlus = Eplus/EGamma, xMinus = Eminus/EGamma;
 G4double thetaPlus = PGamma.angle(Pplus), thetaMinus = PGamma.angle(Pminus);
 G4double GammaPlus = Eplus/particle_mass;
 G4double GammaMinus= Eminus/particle_mass;
 
 G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
 
 if(0.0 == thetaPlus || 0.0 == thetaMinus) {
   G4cout << "SteppingAction for GammaToMuPair: "
          << "thetaPlus= " << thetaPlus << " thetaMinus= " << thetaMinus
          << " gamPlus= " << GammaPlus << " gamMinus= " <<  GammaMinus
          << "  " << thetaPlus *GammaPlus - thetaMinus*GammaMinus << G4endl;
   return;
 }
 analysisManager->FillNtupleDColumn(tuple_index, 0, xPlus);
 analysisManager->FillNtupleDColumn(tuple_index, 1, xMinus);
 analysisManager->FillNtupleDColumn(tuple_index, 2, thetaPlus);
 analysisManager->FillNtupleDColumn(tuple_index, 3, thetaMinus);
 analysisManager->FillNtupleDColumn(tuple_index, 4, sqrt(pow(ETot,2)-pow(PTot.getR(),2)));
 analysisManager->FillNtupleDColumn(tuple_index, 5, Pplus.x());
 analysisManager->FillNtupleDColumn(tuple_index, 6, Pplus.y());
 analysisManager->FillNtupleDColumn(tuple_index, 7, Pplus.z());
 analysisManager->FillNtupleDColumn(tuple_index, 8, Pminus.x());
 analysisManager->FillNtupleDColumn(tuple_index, 9, Pminus.y());
 analysisManager->FillNtupleDColumn(tuple_index, 10, Pminus.z());
 analysisManager->FillNtupleDColumn(tuple_index, 11, thetaSep);
 analysisManager->AddNtupleRow(tuple_index);
 
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......


