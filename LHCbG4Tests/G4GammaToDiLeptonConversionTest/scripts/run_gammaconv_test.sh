#!/usr/bin/env sh

echo "Running G4GammaConversionTest..."
python $G4GAMMATODILEPTONCONVERSIONTESTROOT/scripts/G4GammaToDiLeptonConversionTest.py --energies "[10, 100, 1000]" --thickness "[0.3, 1, 10]"
echo "Test Complete."
