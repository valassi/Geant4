#!/usr/bin/env python
# -*- coding: utf-8 -*-

import subprocess
import argparse

from math import log10

import logging

logging.basicConfig()
_logger = logging.getLogger("GAMMA2DILEPTONPROD")
_logger.setLevel("INFO")

parser = argparse.ArgumentParser("DILEPTONPROD")

parser.add_argument("--nevts", default=10000, help="Number of Events")
parser.add_argument("--energies", default="[1000]", help="Energies List")
parser.add_argument("--thickness", default="[0.3]", help="List of thicknesses")
parser.add_argument("--scale", default="100000", help="Scale DiMuon Conversion Cross Section by factor")
parser.add_argument("--debug", default=False, action="store_true", help="Debug Mode")
parser.add_argument("--outputdir", default="G4GammaCVTestROOTFiles", help="Output Location")

args = parser.parse_args()

if args.debug:
    _logger.setLevel("DEBUG")

def get_list(argument):
    import re
    elements = re.findall(r'[\d|\.]+', argument)
    return elements 

energies = get_list(args.energies)
thickness = get_list(args.thickness)

try:
    assert int(args.nevts) > 0
except AssertionError:
    _logger.error("Number of Events Must be a Positive Integer")
    exit(0)
except TypeError:
    _logger.error("Invalid Input for Number of Events")
    exit(0)

_logger.debug("Creating Test Output Directory...")

subprocess.check_call("mkdir -p {}".format(args.outputdir), shell=True)

for energy in energies:
    for t in thickness:
        _logger.info("%s\n\nRunning Gamma->l+ l- Conversion in Aluminium with options:\n"+
                     "\n\tNumber of Events         :\t%s "+
                     "\n\tBeam Energy              :\t%s GeV"+
                     "\n\tTarget Thickness         :\t%s mm"+
                     "\n\tDiMuon XSec Scale Factor :\t%s"+
                     "\n\tOutput Directory         :\t%s\n", 
         '='*40, args.nevts, energy, t, args.scale, args.outputdir)
        _logger.info('='*40)
        subprocess.check_call("G4GammaToDiLeptonConversionTest.exe --nevts {} --energy {}".format(args.nevts, energy) +
                              " --thickness {} --scale {} --outputdir {}".format(t, args.scale, args.outputdir), shell=True)

_logger.info("Creating Single Combined Plots ROOT File...")

import ROOT

_output = ROOT.TFile.Open("G4GammaCVTestROOTFiles/G4GammaToDiLeptonConversionTest.root", "NEW")

_histo_dim_dict = {'DiElectron' : {'Hist' : {'InvMass'    : (100, 0, 100),
                                             'ThetaSep' : (50, 0, 6E-4),
                                             'InvMass2D' : (100, 1, 3),
                                             'Theta'  : (100, 0, 0.02),
                                             'EFrac' : (100,0,1)},
                                   'Symbol' : 'e'},
                   'DiMuon'     : {'Hist' : {'InvMass'    : (150, 250, 550),
                                             'Theta'  : (100, 0, 0.07), 
                                             'ThetaSep' : (50, 0, 1E-1),
                                             'InvMass2D' : (50, 200, 500),
                                             'EFrac' : (100,0,1)},
                                   'Symbol' : '#mu' } }

for energy in energies:
    for t in thickness:
        _logger.debug("Opening file '%s'", "{}/TestEM6_Al_{}mm_{}GeV_{}.root".format(args.outputdir, t, energy, args.nevts))
        _tmp = ROOT.TFile.Open("{}/TestEM6_Al_{}mm_{}GeV_{}.root".format(args.outputdir, t, energy, args.nevts))
  
        for particle in _histo_dim_dict.keys():
             _logger.debug("Attempting to retrieve TTree '%s'", particle)
             _tree = _tmp.Get(particle)
             assert _tree.GetEntries() > 0, "No Entries Found!"
        
             hist_mm = ROOT.TH1F("{}_InvMass_{}GeV_{}mm_Al".format(particle, energy, t),
                                 "{} Invariant Mass from {}GeV Photons in {}mm of Aluminium".format(particle, energy, t),
                                 *_histo_dim_dict[particle]['Hist']['InvMass'])
             hist_mm.GetXaxis().SetTitle("M_{%s}/MeV" % '{s}{s}'.format(s=_histo_dim_dict[particle]['Symbol']))
             

             hist_tp = ROOT.TH1F("{}_Angle_{}GeV_{}mm_Al".format(particle, energy, t),
                                 "#gamma-{} Angle for {}GeV Photons in {}mm of Aluminium".format(_histo_dim_dict[particle]['Symbol']+'^{#pm}', energy, t),
                                 *_histo_dim_dict[particle]['Hist']['Theta'])
             hist_tp.GetXaxis().SetTitle("#theta(p_{%s}, p_{%s})" % ('#gamma', _histo_dim_dict[particle]['Symbol']+'^{#pm}'))

             hist_efp = ROOT.TH1F("{}_EnergyFrac_{}GeV_{}mm_Al".format(particle, energy, t),
                                  "Energy Fraction of {} for {}GeV Photons in {}mm of Aluminium".format(_histo_dim_dict[particle]['Symbol']+'^{#pm}', energy, t),
                                  *_histo_dim_dict[particle]['Hist']['EFrac'])
             hist_efp.GetXaxis().SetTitle("E_{%s}/E_{%s}" % (_histo_dim_dict[particle]['Symbol']+'^{#pm}', "#gamma"))
             
             hist_2d_sep_ang_mm = ROOT.TH2F("{}_Separation_Angle_vs_InvMass_{}GeV_{}mm_Al".format(particle, energy, t),
                                            "Separation Angle Between {} Pair vs Invariant Mass for {}GeV Photons in {}mm of Aluminium".format(particle, energy, t),
                                       _histo_dim_dict[particle]['Hist']['InvMass2D'][0],
                                       _histo_dim_dict[particle]['Hist']['InvMass2D'][1],
                                       _histo_dim_dict[particle]['Hist']['InvMass2D'][2], 
                                       _histo_dim_dict[particle]['Hist']['ThetaSep'][0],
                                       _histo_dim_dict[particle]['Hist']['ThetaSep'][1],
                                       10*_histo_dim_dict[particle]['Hist']['ThetaSep'][2]/float(energy))

             hist_2d_sep_ang_mm.GetXaxis().SetTitle("M_{%s}/MeV" % '{s}{s}'.format(s=_histo_dim_dict[particle]['Symbol']))
             hist_2d_sep_ang_mm.GetYaxis().SetTitle("#theta(p_{%s},p_{%s})" % (_histo_dim_dict[particle]['Symbol']+'^{+}',
                                                                                       _histo_dim_dict[particle]['Symbol']+'^{-}'))
             hist_2d_sep_ang_mm.GetYaxis().SetTitleOffset(1.2)

             for event in _tree: # ROOT broke when using Draw(hist >> hist_tmp(i,j,k)) method
                 hist_mm.Fill(getattr(event, '{}_InvMass'.format(particle)))
                 hist_tp.Fill(getattr(event, '{}_ThetaPlus'.format(particle)))
                 hist_tp.Fill(getattr(event, '{}_ThetaMinus'.format(particle)))
                 hist_efp.Fill(getattr(event, '{}_EnergyFracPlus'.format(particle)))
                 hist_efp.Fill(getattr(event, '{}_EnergyFracMinus'.format(particle)))
                 hist_2d_sep_ang_mm.Fill(getattr(event, '{}_InvMass'.format(particle)), getattr(event, '{}_ThetaSep'.format(particle)))

             _output.cd()
 
             hist_mm.Write(hist_mm.GetName(),ROOT.TObject.kOverwrite)
             hist_tp.Write(hist_tp.GetName(),ROOT.TObject.kOverwrite)
             hist_efp.Write(hist_efp.GetName(),ROOT.TObject.kOverwrite)
             hist_2d_sep_ang_mm.Write(hist_2d_sep_ang_mm.GetName(),ROOT.TObject.kOverwrite)
             
             hist_mm.Delete()
             hist_tp.Delete()
             hist_efp.Delete()
             hist_2d_sep_ang_mm.Delete()
          
        _tmp.Close()

_output.Write()
_output.Close()

