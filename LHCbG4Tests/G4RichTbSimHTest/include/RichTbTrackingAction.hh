#ifndef RichTbTrackingAction_h
#define RichTbTrackingAction_h 1

#include "Geant4/G4UserTrackingAction.hh"
#include "Geant4/G4Track.hh"

class RichTbTrackingAction : public G4UserTrackingAction {

public:
  RichTbTrackingAction();
  virtual ~RichTbTrackingAction();
  void PreUserTrackingAction(const G4Track *aTrack) override;

  void PostUserTrackingAction(const G4Track *aTrack) override;

private:
};
#endif
