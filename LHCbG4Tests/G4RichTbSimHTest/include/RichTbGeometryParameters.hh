#ifndef RichTbGeometryParameters_h
#define RichTbGeometryParameters_h 1

#include "Geant4/globals.hh"
#include <math.h>

extern void InitializeRichTbGeometry();
//
//

static const G4double sqroot3 = pow(3.0, 0.5);

#include "RichTbHallGeometryParameters.hh"
#include "RichTbVesselGeometryParameters.hh"
#include "RichTbCrystalGeometryParameters.hh"
#include "RichTbCrystalMasterGeometryParameters.hh"
#include "RichTbRadiatorGeometryParameters.hh"
#include "RichTbLensGeometryParameters.hh"
#include "RichTbMasterGeometryParameters.hh"

#include "RichTbCrystalCoverGeometryParameters.hh"
#include "RichTbMirrorGeometryParamters.hh"
#include "RichTbDarkCoverGeometryParameters.hh"

#include "RichTbPMTGeometryParameters.hh"
#include "RichTbPMTSupportFrameGeometryParameters.hh"

#include "RichTbHpdGeometryParameters.hh"

#include "RichTbExtHpdGeometryParameters.hh"
#include "RichTbHpdSupportFrameGeometryParameters.hh"

#include "RichTbPhDFrameGeometryParameters.hh"

#include "RichTbBeamGeometryParameters.hh"

//

#endif /*RichTbGeometryParameters_h */
