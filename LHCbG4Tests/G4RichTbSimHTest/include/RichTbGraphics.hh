#ifndef RichTbGraphics_h
#define RichTbGraphics_h 1

#include "Geant4/globals.hh"
#include "Geant4/G4VUserDetectorConstruction.hh"
#include "Geant4/G4VPhysicalVolume.hh"
#include "RichTbUpgradeDetectorConstruction.hh"

class RichTbGraphics {

public:
  RichTbGraphics();
  RichTbGraphics(RichTbDetectorConstruction *);
  virtual ~RichTbGraphics();

  void setAllGraphicsAttributes();
  void setRichTbHallGraphicsAttibutes();

private:
  RichTbDetectorConstruction *curDetector;
};
#endif /*RichTbGraphics_h */
