#include "RichTbEventAction.hh"
#include "Geant4/G4Event.hh"
#include "Geant4/G4EventManager.hh"
#include "Geant4/G4HCofThisEvent.hh"
#include "Geant4/G4SDManager.hh"
#include "Geant4/G4Trajectory.hh"
#include "Geant4/G4TrajectoryContainer.hh"
#include "Geant4/G4UImanager.hh"
#include "Geant4/G4VHitsCollection.hh"
#include "Geant4/G4VVisManager.hh"
#include "Geant4/G4ios.hh"
#include "RichTbVisManager.hh"
//#include "RichTbSD.hh"
#include "RichTbAnalysisManager.hh"
#include "RichTbCounter.hh"
#include "RichTbHit.hh"
#include "RichTbIOData.hh"
#include "RichTbMiscNames.hh"
#include "RichTbRunConfig.hh"

RichTbEventAction::RichTbEventAction() {
  RichTbCollID = -1;
  RichTbCollIDHpd = -1;
}

RichTbEventAction::~RichTbEventAction() {}

void RichTbEventAction::BeginOfEventAction(const G4Event * /*  evt */) {

  // reset the counters.

  RichTbCounter *aRichCounter = RichTbCounter::getRichTbCounterInstance();
  aRichCounter->resetRichTbCounter();

  G4SDManager *SDman = G4SDManager::GetSDMpointer();
  if (RichTbCollID < 0) {
    G4String colNam;
    RichTbCollID = SDman->GetCollectionID(colNam = RichTbHColname);
  }
  RichTbRunConfig *RConfig = RichTbRunConfig::getRunConfigInstance();
  G4int aRadiatorConfiguration = RConfig->getRadiatorConfiguration();

  if (aRadiatorConfiguration == 2) {

    if (RichTbCollIDHpd < 0) {
      G4String colNamh;
      RichTbCollIDHpd = SDman->GetCollectionID(colNamh = RichTbHColnameHpd);
    }
  }
}

void RichTbEventAction::EndOfEventAction(const G4Event *evt) {
  RichTbRunConfig *RConfig = RichTbRunConfig::getRunConfigInstance();
  if (RichTbCollID < 0)
    return;
  // first get the trajectories
  G4TrajectoryContainer *trajectoryContainer = evt->GetTrajectoryContainer();
  G4int n_trajectories = 0;
  if (trajectoryContainer) {
    n_trajectories = trajectoryContainer->entries();
    G4cout << "     " << n_trajectories << " Tracks are stored in Trajectorycontainer." << G4endl;
  }
  // Now get the hits

//  G4HCofThisEvent *HCE = evt->GetHCofThisEvent();
//  RichTbHitsCollection *RHC = NULL;
//  if (HCE) {
//    RHC = (RichTbHitsCollection *)(HCE->GetHC(RichTbCollID));
//  }

//  if (RHC) {
//    G4int n_hit = RHC->entries();
    // G4cout << "     " << n_hit << " hits are stored in RichTbHitsCollection." << G4endl;
//  }
  // Now for analysis of hits related information.

  RichTbAnalysisManager *aAnalysisManager = RichTbAnalysisManager::getInstance();
  // aAnalysisManager->EndOfEventCountersHisto(evt);
  aAnalysisManager->EndOfEventHisto(evt);

  // Now to write out the data

  if (RConfig->DoWriteOutputFile()) {

    RichTbIOData *rTbIOData = RichTbIOData::getRichTbIODataInstance();

    rTbIOData->WriteOutEventHeaderData(evt);
    rTbIOData->WriteOutHitData(evt);
    //  G4cout << ">>> End of Event  Number  " << evt->GetEventID() << G4endl;
  }
  G4int totNumev = RConfig->getNumEventInBatchMode();
  if (totNumev > 100) {
    if ((evt->GetEventID()) % (totNumev / 100) == 0) {
      G4cout << ">>> End of Event  Number  " << evt->GetEventID() << G4endl;
    }
  } else {
    G4cout << ">>> End of Event  Number  " << evt->GetEventID() << G4endl;
  }
}
