#include "RichTbPrimaryGeneratorAction.hh"

#include "Geant4/G4Event.hh"
#include "Geant4/G4ParticleDefinition.hh"
#include "Geant4/G4ParticleGun.hh"
#include "Geant4/G4ParticleTable.hh"
#include "Geant4/Randomize.hh"
#include "Geant4/globals.hh"
#include "RichTbAnalysisManager.hh"
#include "RichTbBeamProperty.hh"
#include "RichTbGeometryParameters.hh"
#include "RichTbMaterialParameters.hh"
#include "RichTbRunConfig.hh"
#include <math.h>

RichTbPrimaryGeneratorAction::RichTbPrimaryGeneratorAction() {
  // RichTbRunConfig* rConfig =  RichTbRunConfig::getRunConfigInstance();

  G4int n_particle = 1;
  particleGun = new G4ParticleGun(n_particle);
}

RichTbPrimaryGeneratorAction::~RichTbPrimaryGeneratorAction() {
  if (particleGun != 0)
    delete particleGun;
}

void RichTbPrimaryGeneratorAction::GeneratePrimaries(G4Event *anEvent) {
  RichTbBeamProperty *aBeamProperty = RichTbBeamProperty::getRichTbBeamPropertyInstance();
  aBeamProperty->ResetBeamProperty();
  RichTbRunConfig *rConfig = RichTbRunConfig::getRunConfigInstance();
  G4int NumPart = rConfig->getRichTbNumPartEvent();
  for (G4int ip = 0; ip < NumPart; ip++) {
    CurrentBeamParticle = SetParticleType();
    particleGun->SetParticleTime(0.0 * CLHEP::ns);
    SetParticleStartPos();
    SetParticleDirection();
    SetParticleKineticEnergy(CurrentBeamParticle);
    particleGun->GeneratePrimaryVertex(anEvent);
  }
}

G4ParticleDefinition *RichTbPrimaryGeneratorAction::SetParticleType() {
  RichTbRunConfig *rConfig = RichTbRunConfig::getRunConfigInstance();
  RichTbBeamProperty *aBeamProperty = RichTbBeamProperty::getRichTbBeamPropertyInstance();

  G4int patype = rConfig->getRichTbParticleTypeCode();
  // default is with patyp =0 which is for pi-.
  // the pi- beam is set to be not a mixture, for now.
  // the pi+ beam is a mixture of pi+ and protons.

  G4String particleName = "pi-";

  if (patype == 1) {
    particleName = "opticalphoton";

  } else if (patype == 2) {
    G4double PosFr = rConfig->getPosBeamFraction();
    G4double randomQut1 = G4UniformRand();
    if (randomQut1 > PosFr) {
      particleName = "pi+";
    } else {
      particleName = "proton";
    }

  } else if (patype == 3) {

    G4double ElnFr = rConfig->getPosBeamFraction();
    G4double randomQut1 = G4UniformRand();
    if (randomQut1 > ElnFr) {
      particleName = "pi-";
    } else {
      particleName = "e-";
    }

  } else if (patype == 4) {

    G4double KaonFr = rConfig->getPosBeamFraction();
    G4double randomQutk = G4UniformRand();
    if (randomQutk > KaonFr) {
      particleName = "pi-";
    } else {
      particleName = "kaon-";
    }

  } else if (patype == 5) {

    G4double KaonFrp = rConfig->getPosBeamFraction();
    G4double randomQutp = G4UniformRand();
    if (randomQutp > KaonFrp) {
      particleName = "pi+";
    } else {
      particleName = "kaon+";
    }

  } else if (patype == 6) {
    particleName = "proton";
  } else if (patype == 7) {
    particleName = "mu-";
  }

  G4ParticleTable *particleTable = G4ParticleTable::GetParticleTable();
  G4ParticleDefinition *particle = particleTable->FindParticle(particleName);

  particleGun->SetParticleDefinition(particle);
  // G4cout<<" Current Beam particle is  "<<particleName<<G4endl;

  // now store the info.
  aBeamProperty->setBeamPartDef(particle);
  aBeamProperty->setBeamPartName(particleName);

  return particle;
}

void RichTbPrimaryGeneratorAction::SetParticleStartPos() {

  RichTbRunConfig *rConfig = RichTbRunConfig::getRunConfigInstance();
  RichTbBeamProperty *aBeamProperty = RichTbBeamProperty::getRichTbBeamPropertyInstance();
  G4ThreeVector positionStd = aBeamProperty->getNominalBeamPosition();

  G4int PosCode = rConfig->getRichTbParticleStartPosCode();
  // standard positon is at PosCode =0.
  //  G4double beamPosXStd = AgelTileXLocation[0];
  //  G4double beamPosYStd = AgelTileYLocation[0]+0.5*RichTbVesselYSize;
  //  G4double beamPosZStd=-200.0*CLHEP::mm;
  //    G4ThreeVector positionStd(0.0*cm,0.0*cm,-200.0*CLHEP::mm);
  //  G4ThreeVector positionStd( beamPosXStd,beamPosYStd, beamPosZStd);

  //  G4cout<<" BeamPos code PosStdXYZ"<< PosCode<<"  "
  //      <<positionStd.x()<<"  "<<positionStd.y()<<"  "<<positionStd.z()
  //      <<G4endl;

  if (PosCode == 0) {
    particleGun->SetParticlePosition(positionStd);
    // store the positon for writeOut.

    aBeamProperty->setBeamPosition(positionStd);

  } else if (PosCode == 1) {

    G4double aBeamXShift = rConfig->getBeamXPosShift();
    G4double aBeamYShift = rConfig->getBeamYPosShift();
    G4double aBeamPosX = positionStd.x() + aBeamXShift;
    G4double aBeamPosY = positionStd.y() + aBeamYShift;
    G4double aBeamPosZ = positionStd.z();

    G4ThreeVector aBeamPosition(aBeamPosX, aBeamPosY, aBeamPosZ);
    particleGun->SetParticlePosition(aBeamPosition);
    // store the positon for writeOut.
    // G4cout<<" Current beam part start pos is "
    //   << aBeamPosX<<"  "<<  aBeamPosY<<"  "<< aBeamPosZ<<G4endl;

    aBeamProperty->setBeamPosition(aBeamPosition);

  } else if (PosCode == 2) {
    //

    G4double aSpBeamX = rConfig->getBeamSpecialXPos();
    G4double aSpBeamY = rConfig->getBeamSpecialYPos();
    G4double aSpBeamZ = rConfig->getBeamSpecialZPos();

    G4ThreeVector aSpBeam(aSpBeamX, aSpBeamY, aSpBeamZ);
    particleGun->SetParticlePosition(aSpBeam);

    // store the positon for writeOut.

    // G4cout<<" Current beam part start pos is "
    //   << aSpBeamX<<"  "<<  aSpBeamY<<"  "<< aSpBeamZ<<G4endl;

    aBeamProperty->setBeamPosition(aSpBeam);

  } else if (PosCode == 3) {

    // make random beam pos along X axis to study the Gap betwen aerogel tiles.
    G4double aSpBeamXInit = rConfig->getBeamSpecialXPos();
    G4double aSpBeamYInit = rConfig->getBeamSpecialYPos();
    G4double aSpBeamZInit = rConfig->getBeamSpecialZPos();

    //     G4double aSpBeamXRange = 8.0*CLHEP::mm;
    G4double aSpBeamXRange = rConfig->getBeamSpecialXRange();
    G4double rands = G4UniformRand();
    // set up a flat random number from -1 to 1.
    G4double aSpBeamXCurrent = aSpBeamXInit + ((rands - 0.5) * 2.0) * aSpBeamXRange;
    G4ThreeVector aSpBeam(aSpBeamXCurrent, aSpBeamYInit, aSpBeamZInit);
    particleGun->SetParticlePosition(aSpBeam);
    // store the positon for writeOut.
    // G4cout<<" Current beam part start pos is "
    //   << aSpBeamXCurrent<<"  "<<  aSpBeamYInit<<"  "
    //      << aSpBeamZInit<<G4endl;

    aBeamProperty->setBeamPosition(aSpBeam);
  }
}
void RichTbPrimaryGeneratorAction::SetParticleKineticEnergy(G4ParticleDefinition *CurPart) {
  RichTbRunConfig *rConfig = RichTbRunConfig::getRunConfigInstance();
  //  RichTbAnalysisManager* rAnalysis =  RichTbAnalysisManager::getInstance();
  //  G4double paMass = CurPart->GetPDGMass();
  // G4cout<<"Cur beam paticle mass " <<paMass<<G4endl;
  G4int PEnergyCode = rConfig->getRichTbParticleEnergyCode();
  G4int curpatype = rConfig->getRichTbParticleTypeCode();
  G4double paMass = 0.0;

  if (curpatype != 1) {

    paMass = CurPart->GetPDGMass();
  }

  if (PEnergyCode == 0) {

    G4double particleMom = (rConfig->getRichTbParticleMomentum()) * CLHEP::GeV;
    G4double PKineticEnergy = pow((particleMom * particleMom + paMass * paMass), 0.5) - paMass;
    particleGun->SetParticleEnergy(PKineticEnergy);

    //   G4cout<<"Current beam Name momentum mass and KE are  "
    //        <<CurPart->GetParticleName()<<"  "<< particleMom
    //  <<"  "<<paMass <<"  "<<  PKineticEnergy <<G4endl;

  } else if (PEnergyCode == 1 && curpatype == 1) {
    // for photons get the wavelength.

    G4double particlewlen = (rConfig->getConstPhotWlenBeam()) * CLHEP::nanometer;

    G4double photonparticleMom = 3.0 * CLHEP::eV;
    if (particlewlen != 0.0) {
      photonparticleMom = (PhotWaveLengthToMom / particlewlen);
    }
    G4double PhotonKineticEnergy = photonparticleMom;

    particleGun->SetParticleEnergy(PhotonKineticEnergy);

    //    G4cout<<"Current beam Photon momentum and wlen are "
    // <<"  "<<  photonparticleMom <<"  "<<  particlewlen  <<G4endl;

  } else if (PEnergyCode == 2 && curpatype == 1) {

    G4double photonparticleMinMom = 1.3 * CLHEP::eV;
    G4double photonparticleMaxMom = 6.5 * CLHEP::eV;
    G4double photonMinWlen = (rConfig->getPhotWlenMinBeam()) * CLHEP::nanometer;
    G4double photonMaxWlen = (rConfig->getPhotWlenMaxBeam()) * CLHEP::nanometer;
    if (photonMinWlen != 0.0) {
      photonparticleMaxMom = PhotWaveLengthToMom / photonMinWlen;
    }
    if (photonMaxWlen != 0.0) {
      photonparticleMinMom = PhotWaveLengthToMom / photonMaxWlen;
    }

    G4double randa = G4UniformRand();
    G4double CurPhotMom = photonparticleMinMom + randa * (photonparticleMaxMom - photonparticleMinMom);
    G4double curPhotonKineticEnergy = CurPhotMom;

    particleGun->SetParticleEnergy(curPhotonKineticEnergy);

    // G4double curPhotonWlenNano = 0.0;
    //  if( CurPhotMom != 0.0 ) {

    //   curPhotonWlenNano = PhotMomToWaveLength /(CurPhotMom*CLHEP::nanometer);
    // }

    // if( rAnalysis ->getfhistoWClarityProduced() ) {
    //  rAnalysis ->getfhistoWClarityProduced()->fill(curPhotonWlenNano,1.0);

    //  }

    //      G4cout<<"Current beam Photon momentum and wavlength are "
    //     <<"  "<< CurPhotMom <<"  "<<curPhotonWlenNano<<G4endl;
  }
}

void RichTbPrimaryGeneratorAction::SetParticleDirection() {
  RichTbRunConfig *rConfig = RichTbRunConfig::getRunConfigInstance();
  RichTbBeamProperty *aBeamProperty = RichTbBeamProperty::getRichTbBeamPropertyInstance();
  // default direction code is 0 for 001 direction with no
  // misalignment or divergence.
  G4int DirCode = rConfig->getRichTbParticleDirectionCode();
  //  G4ThreeVector direction(0.0,0.0,1.0)
  G4ThreeVector directionStd = aBeamProperty->getNominalBeamDirectionCos();

  G4ThreeVector direction = directionStd;

  G4double aBeamDirX = rConfig->getBeamDirX();
  G4double aBeamDirY = rConfig->getBeamDirY();
  G4double aBeamDiverX = rConfig->getBeamDivergenceX();
  G4double aBeamDiverY = rConfig->getBeamDivergenceY();

  if (DirCode == 1) {
    G4ThreeVector aDirection(directionStd.x(), directionStd.y(), -1.0 * directionStd.z());
    direction = aDirection;
  } else if (DirCode == 2) {
    // with misalignment and no divergence.

    G4ThreeVector bDirection(aBeamDirX, aBeamDirY, 1.0);
    direction = bDirection;

  } else if (DirCode == 3) {
    // with misalignment and divergence.

    G4double pxBeA = G4RandGauss::shoot(aBeamDirX, aBeamDiverX);
    G4double pyBeA = G4RandGauss::shoot(aBeamDirY, aBeamDiverY);
    G4double thetaBeam = pow((pxBeA * pxBeA + pyBeA * pyBeA), 0.5);
    G4double phiBeam = 0.0;

    if (thetaBeam != 0.) {
      phiBeam = acos(pxBeA / thetaBeam);
      if (pyBeA < 0.)
        phiBeam = -phiBeam;
    }
    G4double pxBe = sin(thetaBeam) * cos(phiBeam);
    G4double pyBe = sin(thetaBeam) * sin(phiBeam);
    G4double pzBe = cos(thetaBeam);

    G4ThreeVector cDirection(pxBe, pyBe, pzBe);
    direction = cDirection;

  } else if (DirCode == 4) {

    G4double aslpx = aBeamDirX / 1.0;
    G4double aslpy = aBeamDirY / 1.0;

    G4double rslpX = G4RandGauss::shoot(aslpx, aBeamDiverX);
    G4double rslpY = G4RandGauss::shoot(aslpy, aBeamDiverY);

    G4double rsfc = pow((1.0 + rslpX * rslpX + rslpY * rslpY), 0.5);
    G4double pxBe = 0.0;
    G4double pyBe = 0.0;
    G4double pzBe = 1.0;
    if (rsfc != 0.0) {
      pzBe = 1.0 / rsfc;
      pxBe = rslpX / rsfc;
      pyBe = rslpY / rsfc;
    }
    G4ThreeVector cDirection(pxBe, pyBe, pzBe);
    direction = cDirection;
  }
  particleGun->SetParticleMomentumDirection(direction.unit());

  // G4cout<<" Current Beam direction is   "<<direction.x()
  //   <<"    "<<direction.y()<<"    "<<direction.z()<<G4endl;

  G4int patypa = rConfig->getRichTbParticleTypeCode();

  if (patypa == 1) {
    G4double rand = G4UniformRand();
    G4double phi = 2 * M_PI * rand;
    G4double sx = cos(phi);
    G4double sy = sin(phi);
    G4double sz = 0.0;
    G4ThreeVector polarization(sx, sy, sz);

    particleGun->SetParticlePolarization(polarization);
  }
  // store the beam direction for writeOut.

  aBeamProperty->setBeamDirection(direction);
}
