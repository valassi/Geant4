// Geant4 headers
#include "Geant4/G4Box.hh"
#include "Geant4/G4LogicalVolume.hh"
#include "Geant4/G4PVPlacement.hh"
#include "Geant4/G4SDManager.hh"
#include "Geant4/G4ThreeVector.hh"
#include "Geant4/G4VPhysicalVolume.hh"

// STL etc
#include <iostream>

// local headers
#include "RichTbGeometryParameters.hh"
#include "RichTbHall.hh"
#include "RichTbMiscNames.hh"
#include "RichTbUpgradeDetectorConstruction.hh"

#include "RichTbSurface.hh"
#include "RichTbSurfaceDefinition.hh"

#include "RichTbUpgradeHpdSD.hh"
#include "RichTbUpgradeSD.hh"

RichTbDetectorConstruction::RichTbDetectorConstruction() { ; }

RichTbDetectorConstruction::~RichTbDetectorConstruction() { ; }

G4VPhysicalVolume *RichTbDetectorConstruction::Construct() {

  // now create all the materials and surface properties.
  // RichTbMaterial* rMaterial = RichTbMaterial::getRichTbMaterialInstance();
  RichTbRunConfig *aConfig = RichTbRunConfig::getRunConfigInstance();
  G4int aRadiatorConfiguration = aConfig->getRadiatorConfiguration();

  G4int CurNumPmts = NumPmtsOld;
  //	G4int CurNumPmts = RichTbPMTGeometryParameters::getNumPmts(); //TESTnumPmts
  if (aRadiatorConfiguration == 2)
    CurNumPmts = NumPmtsWithHpd;
  else if (aRadiatorConfiguration == 3)
    CurNumPmts = NumPmtsUpgrade15;
  //	else if(aRadiatorConfiguration == 3) CurNumPmts = RichTbPMTGeometryParameters::getNumPmtsUpgrade();//TESTnumPmts

  RichTbSurfaceDefinition *rSurfaceDef = RichTbSurfaceDefinition::getRichTbSurfaceDefinitionInstance();
  if (rSurfaceDef)
    G4cout << " RichTb Surface definitions created " << G4endl;

  InitializeRichTbGeometry();

  rTbHall = new RichTbHall();

  rTbVessel = new RichTbUpgradeVessel(rTbHall);

  rTbCrystalMaster = new RichTbUpgradeCrystalMaster(rTbVessel);

  if (aRadiatorConfiguration == 0) {

    rTbCrystal = new RichTbCrystal(rTbCrystalMaster);

    rTbLens = new RichTbLens(rTbCrystalMaster);

    rTbCrystalCover = new RichTbCrystalCover(rTbCrystalMaster);

  } else {
    // Radiator + Mirror + Cover
    rTbUpgradeRadiator = new RichTbUpgradeRadiator(rTbCrystalMaster);
    rTbUpgradeMirror = new RichTbUpgradeMirror(rTbCrystalMaster, rTbUpgradeRadiator);
    // rTbMirror = RichTbUpgradeMirror::getRichTbUpgradeMirrorInstance( rTbVessel );
    rTbUpgradeDarkCover = new RichTbUpgradeDarkCover(rTbUpgradeRadiator);

    if (aRadiatorConfiguration == 3) {
      rTbUpgradeDarkCover->constructRichTbUpgradeUpsDarkCoverEnvelope15(rTbCrystalMaster);
      rTbUpgradeDarkCover->constructRichTbUpgradeSideDarkCoverEnvelope15(rTbCrystalMaster);
    } else {
      rTbUpgradeDarkCover->constructRichTbUpgradeUpsDarkCoverEnvelope(rTbCrystalMaster);
      rTbUpgradeDarkCover->constructRichTbUpgradeSideDarkCoverEnvelope(rTbCrystalMaster);
    }

    // Elementary Cells, PMT's and Support
    rTbUpgradePhotSupFrame = new RichTbUpgradePhDetSupFrame(rTbCrystalMaster);
    if (aRadiatorConfiguration == 1) {
      rTbUpgradePhotSupFrame->constructRichTbPhotoDetectorSupFrame();
    } else if (aRadiatorConfiguration == 2) {
      rTbUpgradePhotSupFrame->constructRichTbPhotoDetectorSupFrameWithHpd();
    } else if (aRadiatorConfiguration == 3) {
      rTbUpgradePhotSupFrame->constructRichTbPhotoDetectorSupFrame15();
    }

    rTbEC = new RichTbUpgradeEC(rTbUpgradePhotSupFrame);

    if (aRadiatorConfiguration == 1) {
      rTbEC->constructRichTbUpgradeEC();
      rTbEC->constructRichTbUpgradeECSupport();
    } else if (aRadiatorConfiguration == 2) {
      rTbEC->constructRichTbUpgradeSingleEC();
      rTbEC->constructRichTbUpgradeSingleECSupport();
    } else if (aRadiatorConfiguration == 3) {
      rTbEC->constructRichTbUpgradeEC15();
      rTbEC->constructRichTbUpgradeECSupport15();
    }

    rTbPMT = new RichTbPMT(rTbEC);
    rTbPMT->buildPMTGeometry();
    if (aRadiatorConfiguration == 2) {
      rTbHpd = new RichTbHpd(0, rTbUpgradePhotSupFrame);
      rTbHpd->buildHpdGeometry();
    }
  }

  // now for the surfaces

  rTbSurface = new RichTbSurface(this);

  // sensitive detector creation for PMT

  G4SDManager *SDman = G4SDManager::GetSDMpointer();

  RichTbUpgradeSD *PMTSD = new RichTbUpgradeSD(PMTSDname);

  // sensitive detector creation for HPD
  RichTbUpgradeHpdSD *HPDSD = (aRadiatorConfiguration == 2) ? new RichTbUpgradeHpdSD(HPDSDname) : 0;

  // now for the readout Geometry.

  G4cout << " Now for readout geometry in Pmt " << G4endl;

  rTbROGeom = new RichTbROGeometry(ROgeometryName, this);

  rTbROGeom->BuildROGeometry();
  PMTSD->SetROgeometry(rTbROGeom);
  SDman->AddNewDetector(PMTSD);
  G4cout << " Now associate sensdet to  readout geometry in Pmt " << G4endl;
  // begin test
  G4int HCID = G4SDManager::GetSDMpointer()->GetCollectionID(RichTbHColname);
  G4cout << " Now associate sensdet to  readout geometry in Pmt " << HCID << G4endl;
  // end test
  //  Associate the anode of each pmt to the sensitive detector.
  for (G4int ipmt = 0; ipmt < CurNumPmts; ipmt++) {

    G4LogicalVolume *RichTbPmtAnode_LV = rTbPMT->getRichTbPMTAnodeLVol()[ipmt];
    RichTbPmtAnode_LV->SetSensitiveDetector(PMTSD);
    G4cout << "associate pmtsd to sensdet " << RichTbPmtAnode_LV->GetName() << "  " << ipmt << G4endl;
  }

  // Now for the HPD sensitive detector
  if (aRadiatorConfiguration == 2) {
    if (HPDSD) {
      G4cout << " Now for readout geometry in hpd " << G4endl;

      // now for the hpd readout
      rTbROGeomHpd = new RichTbROGeometryHpd(ROgeometryNameHpd, this);
      rTbROGeomHpd->BuildROGeometry();

      HPDSD->SetROgeometry(rTbROGeomHpd);
      SDman->AddNewDetector(HPDSD);

      G4cout << "associate hpdsd to sensdet " << G4endl;
      // begin test
      G4int numberOfCollections = HPDSD->GetNumberOfCollections();
      G4cout << "hpdsd test   " << numberOfCollections << "  " << HPDSD->GetName() << "  "
             << HPDSD->GetCollectionName(0) << G4endl;

      G4int HCIDHA = G4SDManager::GetSDMpointer()->GetCollectionID(RichTbHColnameHpd);

      G4cout << "associated hpdsd to sensdet A  " << HCIDHA << G4endl;
      // end test

      G4LogicalVolume *RichTbHpdAnode_LV = rTbHpd->getRichTbHpdSiDetLVol();
      RichTbHpdAnode_LV->SetSensitiveDetector(HPDSD);
      G4cout << "associated hpdsd to sensdet " << RichTbHpdAnode_LV->GetName() << "  " << G4endl;

      // begin test
      G4int HCIDH = G4SDManager::GetSDMpointer()->GetCollectionID(RichTbHColnameHpd);

      G4cout << "associated hpdsd to sensdet  " << HCIDH << G4endl;
      // end test
    }
  }

  // Now for the graphics setups.
  RichTbGraphics *rTbGraphics = new RichTbGraphics(this);
  if (rTbGraphics)
    G4cout << " Graphics definitions created " << G4endl;

  return rTbHall->getRichTbHallPhysicalVolume();
}
