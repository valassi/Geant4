#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import subprocess

"""
	Stirring application for the RICH test beam simulation test (aka RichTbSimH).
"""

def main():
	print 'Starting RICH test beam simulation test...'

	workspace = os.path.join(os.sep, os.getcwd(), 'G4RichTbSimHTestOutput')
	if not os.path.exists(workspace):
		os.makedirs(workspace)

	os.chdir(workspace)
	print 'Working directory:', os.getcwd()

	print 'Executing the test...'
	cmd = 'G4RichTbSimHTest.exe'
	subprocess.call(cmd, shell=True)


if __name__ == '__main__':
	main()

#EOF
